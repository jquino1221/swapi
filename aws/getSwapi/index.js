const https = require('https');
exports.handler = async (event) => {
    let dataString = '';
    const response = await new Promise((resolve, reject) => {
        const req = https.get("https://swapi.py4e.com/api/vehicles/4/", function(res) {
          res.on('data', data => {
            dataString += data;
          });
          
          res.on('end', () => {
            resolve(
                JSON.parse(dataString)
            );
          });
        });
        
        req.on('error', (e) => {
          reject(
              'Something went wrong!'
          );
        });
        
    });
    const map = {
        nombre: response.name,
        modelo: response.model,
        fabricante: response.manufacturer,
        costo_creditos: response.cost_in_credits,
        longitud: response.length,
        velocidad_max: response.max_atmosphering_speed,
        tripulación: response.crew,
        pasajeros: response.passengers,
        capacidad_carga: response.cargo_capacity,
        consumibles: response.consumables,
        clase_vehículo: response.vehicle_class,
        pilotos: response.pilots,
        pelicula : response.films,
        creado: response.created,
        editado: response.edited,
        url: response.url
    };
    return map;
};