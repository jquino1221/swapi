var mysql = require('mysql');
var config = require('./config.json');

var pool  = mysql.createPool({
    host     : config.dbhost,
    user     : config.dbuser,
    password : config.dbpassword,
    database : config.dbname
  });

exports.handler =  (event, context, callback) => {
  let name = event.name;
  context.callbackWaitsForEmptyEventLoop = false;
  pool.getConnection(function(err, connection) {
	connection.query('update cliente set nombre =' + name +'where id_cliente = 1', 
		function (error, results, fields) {
		connection.release();
		if (error) 
			callback(error);
		else 
			callback(null,results[0].emp_name);
		});
  });
};